#ifndef CURSOR_CONTROL_H__
#define CURSOR_CONTROL_H__

#include <Windows.h>
#include <XnList.h>
#include <math.h>
#include <opencv/cv.h>

using namespace cv;

class CursorControl
{
public:
	CursorControl();
	~CursorControl();
	void MoveMouse(float, float);
	void LeftClick();
	void RightClick();
};

#endif CURSOR_CONTROL_H_