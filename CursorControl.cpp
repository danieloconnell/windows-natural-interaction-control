/****************************************************************************************
*	Author:		Daniel O'Connell	
*	Title:		CursorControl.cpp
*
*	Desc:		Class used for controlling the Windows cursor.
*****************************************************************************************/

#include "CursorControl.h"

CursorControl::CursorControl()
{
}	

CursorControl::~CursorControl()
{
}

void CursorControl::MoveMouse(float x, float y)
{
	if(x>=320 && y<=240)
	{
		x -= 320;

		POINT CurrentMousePos; 
		GetCursorPos(&CurrentMousePos);

		static double SubPixelCarryoverX = 0, SubPixelCarryoverY=0 ;
		static double PrevX = (double)x, PrevY = (double)y; 
		double dx = (double)x - PrevX;
		double dy = (double)y - PrevY;
		double v;

		v = sqrt(dx*dx + dy*dy );
		v = 1.0*v + 0.09*v*v; //acceleration here. v = v + 0.09v^2

		double NewMousePosX = CurrentMousePos.x + SubPixelCarryoverX + dx*v;
		double NewMousePosY = CurrentMousePos.y + SubPixelCarryoverY + dy*v;
		
		SetCursorPos((int)NewMousePosX, (int)NewMousePosY);
	
		SubPixelCarryoverX = ( dx*v - (int)dx*v );
		SubPixelCarryoverY = ( dy*v - (int)dy*v );
	
		PrevX = x;
		PrevY = y;
	}
}

void CursorControl::LeftClick()
{
	INPUT Input={0};
    Input.type = INPUT_MOUSE;

	POINT current;
	GetCursorPos(&current);

	Input.mi.dx = current.x;
	Input.mi.dy = current.y;

	Input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP;

	SendInput(1,&Input,sizeof(INPUT));
}

void CursorControl::RightClick()
{
	INPUT Input={0};
	Input.type = INPUT_MOUSE;

	POINT current;
	GetCursorPos(&current);

	Input.mi.dx = current.x;
	Input.mi.dy = current.y;

	Input.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN | MOUSEEVENTF_RIGHTUP;

	SendInput(1,&Input,sizeof(INPUT));
}
