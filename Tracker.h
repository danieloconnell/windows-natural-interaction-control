//============================================================================
// Name        : Tracker.h
// Author      : Daniel O'Connell
// Description : an approach for fingertip tracking using Xtion Pro
//============================================================================

#ifndef TRACKER_H_
#define TRACKER_H_

#include <iostream>
#include <sstream>
#include <fstream>

#include <opencv/cv.h>
#include <opencv/highgui.h>
using namespace cv;

#include <XnCppWrapper.h>
#include <XnVNite.h>

#include <map>

class Tracker
{
public:
	Tracker();
	~Tracker();

	XnStatus run();

	void createViewer();
private:

	static void XN_CALLBACK_TYPE MyCalibrationInProgress(	xn::SkeletonCapability&, 
													XnUserID, 
													XnCalibrationStatus,
													void*);

	static void XN_CALLBACK_TYPE MyPoseInProgress(	xn::PoseDetectionCapability&,
													const XnChar*,
													XnUserID,
													XnPoseDetectionStatus,
													void*);

	static void XN_CALLBACK_TYPE onNewUser(			xn::UserGenerator&,
													XnUserID,
													void*);

	static void XN_CALLBACK_TYPE User_LostUser(		xn::UserGenerator&,
													XnUserID,
													void*);

	static void XN_CALLBACK_TYPE UserPose_PoseDetected(	xn::PoseDetectionCapability&,
													const XnChar*,
													XnUserID,
													void*);

	static void XN_CALLBACK_TYPE UserCalibration_CalibrationStart(		xn::SkeletonCapability&,
																XnUserID,
																void*);

	static void XN_CALLBACK_TYPE UserCalibration_CalibrationComplete(	xn::SkeletonCapability&,
																XnUserID,
																XnCalibrationStatus,
																void*);
	
	void initXtion(bool, bool, bool);

	float getJointImgCoordinates(const xn::SkeletonCapability&, const XnUserID, const XnSkeletonJoint, float*); 

	bool getHandContour(const Mat&, const float*, vector<Point>&);

	void detectFingerTips(const vector<Point>&, vector<Point>&, Mat*);

	void drawContour(Mat&, const vector<Point>&, const Scalar&);

	double convexity(const vector<Point>&);

	int initTracker();
};

#endif TRACKER_H_