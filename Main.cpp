//============================================================================
// Name        : Main.cpp
// Author      : Daniel O'Connell
// Description : an approach for fingertip tracking using Xtion Pro
//============================================================================

#include "Tracker.h"

Tracker track;

int main()
{
	XnStatus retVal;
	retVal = track.run();
	if(retVal == XN_STATUS_OK)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}