//============================================================================
// Name        : Tracker.cpp
// Author      : Daniel O'Connell
// Description : an approach for fingertip tracking using Xtion Pro
//============================================================================

#include "Tracker.h"
#include "CursorControl.h"
using namespace xn;
using namespace std;

#define CHECK_RC(nRetVal, what)		
/*//////////////////////////////////////////////////////////////////////
// constants
//////////////////////////////////////////////////////////////////////*/

const Size frameSize(640, 480);
const unsigned int maxUsers = 20;
const Mat emptyMat();

/*//////////////////////////////////////////////////////////////////////
// globals
//////////////////////////////////////////////////////////////////////*/

// openNI context and generator nodes
Context context;
ImageGenerator imageGen;
DepthGenerator depthGen;
IRGenerator irGen;
UserGenerator userGen;

unsigned short nearClip = 0;
unsigned short farClip = 0;
unsigned short depth = 0;

XnBool g_bNeedPose = FALSE;
XnChar g_strPose[20] = "";

// Hold number of current finger tips
int tips[4];

// frame buffers
Mat bgrMat(frameSize, CV_8UC3);
Mat depthMat(frameSize, CV_16UC1);
Mat depthMat8(frameSize, CV_8UC1);
Mat DepthMatbgr(frameSize, CV_8UC3);

// Object for controlling the cursor
CursorControl cc;

/*//////////////////////////////////////////////////////////////////////
// callbacks
//////////////////////////////////////////////////////////////////////*/
map<XnUInt32, pair<XnCalibrationStatus, XnPoseDetectionStatus> > m_Errors;
// Callback: For preparing a user in the scene for tracking
void XN_CALLBACK_TYPE Tracker::MyCalibrationInProgress(SkeletonCapability& capability, XnUserID id, XnCalibrationStatus calibrationError, void* pCookie)
{
	m_Errors[id].first = calibrationError;
}
// Callback: During pose detection
void XN_CALLBACK_TYPE Tracker::MyPoseInProgress(PoseDetectionCapability& capability, const XnChar* strPose, XnUserID id, XnPoseDetectionStatus poseError, void* pCookie)
{
	m_Errors[id].second = poseError;
}
// Callback: A new user in the scene
void XN_CALLBACK_TYPE Tracker::onNewUser(UserGenerator& generator, XnUserID nId, void* pCookie)
{
	XnUInt32 epochTime = 0;
	xnOSGetEpochTime(&epochTime);
	printf("%d New User %d\n", epochTime, nId);
	// New user found
	if (g_bNeedPose)
	{
		userGen.GetPoseDetectionCap().StartPoseDetection(g_strPose, nId);
	}
	else
	{
		userGen.GetSkeletonCap().RequestCalibration(nId, TRUE);
	}
}
// Callback: An existing user was lost
void XN_CALLBACK_TYPE Tracker::User_LostUser(UserGenerator& generator, XnUserID nId, void* pCookie)
{
	XnUInt32 epochTime = 0;
	xnOSGetEpochTime(&epochTime);
	printf("%d Lost user %d\n", epochTime, nId);	
}
// Callback: Detected a pose
void XN_CALLBACK_TYPE Tracker::UserPose_PoseDetected(PoseDetectionCapability& capability, const XnChar* strPose, XnUserID nId, void* pCookie)
{
	XnUInt32 epochTime = 0;
	xnOSGetEpochTime(&epochTime);
	printf("%d Pose %s detected for user %d\n", epochTime, strPose, nId);
	userGen.GetPoseDetectionCap().StopPoseDetection(nId);
	userGen.GetSkeletonCap().RequestCalibration(nId, TRUE);
}
// Callback: Started calibration
void XN_CALLBACK_TYPE Tracker::UserCalibration_CalibrationStart(SkeletonCapability& capability, XnUserID nId, void* pCookie)
{
	XnUInt32 epochTime = 0;
	xnOSGetEpochTime(&epochTime);
	printf("%d Calibration started for user %d\n", epochTime, nId);
}
// Callback: Finished calibration
void XN_CALLBACK_TYPE Tracker::UserCalibration_CalibrationComplete(SkeletonCapability& capability, XnUserID nId, XnCalibrationStatus eStatus, void* pCookie)
{
	XnUInt32 epochTime = 0;
	xnOSGetEpochTime(&epochTime);
	if (eStatus == XN_CALIBRATION_STATUS_OK)
	{
		// Calibration succeeded
		printf("%d Calibration complete, start tracking user %d\n", epochTime, nId);		
		userGen.GetSkeletonCap().StartTracking(nId);
	}
	else
	{
		// Calibration failed
		printf("%d Calibration failed for user %d\n", epochTime, nId);
        if(eStatus==XN_CALIBRATION_STATUS_MANUAL_ABORT)
        {
            printf("Manual abort occured, stop attempting to calibrate!");
            return;
        }
		if (g_bNeedPose)
		{
			userGen.GetPoseDetectionCap().StartPoseDetection(g_strPose, nId);
		}
		else
		{
			userGen.GetSkeletonCap().RequestCalibration(nId, TRUE);
		}
	}
}

/*//////////////////////////////////////////////////////////////////////
// functions
//////////////////////////////////////////////////////////////////////*/

void Tracker::initXtion(bool initImage, bool initDepth, bool initUser) {
	XnStatus nRetVal = XN_STATUS_OK;

	// Initialize context object
	nRetVal = context.Init();
	if(nRetVal==-1)
		cout << "init Failed: " << xnGetStatusString(nRetVal) << endl;

	// default output mode
	XnMapOutputMode outputMode;
	outputMode.nXRes = 640;
	outputMode.nYRes = 480;
	outputMode.nFPS = 30;

	// Create an ImageGenerator node
	if (initImage) {	
		nRetVal = imageGen.Create(context);
		if(nRetVal==-1)
			cout << "imageGen.Create Failed: " << xnGetStatusString(nRetVal) << endl;
		nRetVal = imageGen.SetMapOutputMode(outputMode);
		if(nRetVal==-1)
			cout << "imageGen.SetMapOutputMode Failed: " << xnGetStatusString(nRetVal) << endl;
		nRetVal = imageGen.GetMirrorCap().SetMirror(true);
		if(nRetVal==-1)
			cout << "imageGen.GetMirrorCap().SetMirror Failed: " << xnGetStatusString(nRetVal) << endl;
	}

	// Create a DepthGenerator node	
	if (initDepth) {
		nRetVal = depthGen.Create(context);
		if(nRetVal==-1)
			cout << "depthGen.Create Failed: " << xnGetStatusString(nRetVal) << endl;
		nRetVal = depthGen.SetMapOutputMode(outputMode);
		if(nRetVal==-1)
			cout << "depthGen.SetMapOutputMode Failed: " << xnGetStatusString(nRetVal) << endl;
		nRetVal = depthGen.GetMirrorCap().SetMirror(true);
		if(nRetVal==-1)
			cout << "depthGen.GetMirrorCap().SetMirror Failed: " << xnGetStatusString(nRetVal) << endl;
	}

	// create user generator
	if (initUser) {
		nRetVal = userGen.Create(context);
		if(nRetVal==-1)
			cout << "userGen.Create Failed: " << xnGetStatusString(nRetVal) << endl;
		userGen.GetSkeletonCap().SetSkeletonProfile(XN_SKEL_PROFILE_UPPER);
		userGen.GetSkeletonCap().SetSmoothing(0.9);
	}

	// Make it start generating data
	nRetVal = context.StartGeneratingAll();
	if(nRetVal==-1)
		cout << "context.StartGeneratingAll Failed: " << xnGetStatusString(nRetVal) << endl;
}

float Tracker::getJointImgCoordinates(const SkeletonCapability &skeletonCapability, const XnUserID userId, const XnSkeletonJoint skeletonJoint, float *v) {
	XnVector3D projective;
	XnSkeletonJointPosition skeletonJointPosition;
	skeletonCapability.GetSkeletonJointPosition(userId, skeletonJoint, skeletonJointPosition);
	
	depthGen.ConvertRealWorldToProjective(1, &skeletonJointPosition.position, &projective);

	v[0] = projective.X;
	v[1] = projective.Y;
	v[2] = projective.Z / 1000.0f;

	return skeletonJointPosition.fConfidence;
}

bool Tracker::getHandContour(const Mat &depthMat, const float *v, vector<Point> &handContour) {
	const int maxHandRadius = 128; // in px
	const short handDepthRange = 200; // in mm
	const double epsilon = 17.5; // approximation accuracy (maximum distance between the original hand contour and its approximation)

	depth = v[2] * 1000.0f; // hand depth
	nearClip = depth - 100; // near clipping plane
	farClip = depth + 100; // far clipping plane

	static Mat mask(frameSize, CV_8UC1);
	mask.setTo(0);

	// extract hand region	
	circle(mask, Point(v[0], v[1]), maxHandRadius, 255, CV_FILLED);
	mask = mask & depthMat > nearClip & depthMat < farClip;

	// assume largest contour in hand region to be the hand contour
	vector<vector<Point> > contours;
	findContours(mask, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
	int n = contours.size();
	int maxI = -1;
	int maxSize = -1;
	for (int i=0; i<n; i++) {
		int size  = contours[i].size();
		if (size > maxSize) {
			maxSize = size;
			maxI = i;
		}
	}

	bool handContourFound = (maxI >= 0);

	if (handContourFound) {
		approxPolyDP( Mat(contours[maxI]), handContour, epsilon, true );
	}

	return maxI >= 0;
}

void Tracker::detectFingerTips(const vector<Point> &handContour, vector<Point> &fingerTips, Mat *frame = NULL) {
	Mat handContourMat(handContour);
	double area = contourArea(handContourMat);
	Point tips = NULL;

	const Scalar fingerTipColor(255,0,0);

	vector<int> hull;
	convexHull(handContourMat, hull);

	// find upper and lower bounds of the hand and define cutoff threshold (don't consider lower vertices as fingers)
	int upper = 640, lower = 0;
	for (int j=0; j<hull.size(); j++) {
		int idx = hull[j]; // corner index
		if (handContour[idx].y < upper) upper = handContour[idx].y;
		if (handContour[idx].y > lower) lower = handContour[idx].y;
	}
	float cutoff = lower - (lower - upper) * 0.1f;

	// find interior angles of hull corners
	for (int j=0; j<hull.size(); j++) {
		int idx = hull[j]; // corner index
		int pdx = idx == 0 ? handContour.size() - 1 : idx - 1; //  predecessor of idx
		int sdx = idx == handContour.size() - 1 ? 0 : idx + 1; // successor of idx

		Point v1 = handContour[sdx] - handContour[idx];
		Point v2 = handContour[pdx] - handContour[idx];

		float angle = acos( (v1.x*v2.x + v1.y*v2.y) / (norm(v1) * norm(v2)) );

		// low interior angle + within upper 90% of region -> we got a finger
		if (angle < 1 && handContour[idx].y < cutoff) {
			int u = handContour[idx].x;
			int v = handContour[idx].y;

			int size = fingerTips.size();
			fingerTips.push_back(Point2i(u,v));

			// handle cursor actions
			if(fingerTips.size() == size+1) {
				cc.LeftClick();
			} else if(fingerTips.size() == size+2) {
				cc.RightClick();
			} /*else if(fingerTips.size() == size+3 && ) {
				cc.ScrollDown();
			} else if(fingerTips.size() == size+3 && ) {
				cc.ScrollUp();
			}*/

			if (frame) { // draw fingertips
				circle(*frame, handContour[idx], 10, fingerTipColor, -1);
			}
		}
	}
}

void Tracker::drawContour(Mat &img, const vector<Point> &contour, const Scalar &color) {
	vector<vector<Point> > contours;
	contours.push_back(contour);
	drawContours(img, contours, -1, color);
}

double Tracker::convexity(const vector<Point> &contour) {
	Mat contourMat(contour);

	vector<int> hull;
	convexHull(contourMat, hull);

	int n = hull.size();
	vector<Point> hullContour;

	for (int i=0; i<n; i++) {
		hullContour.push_back(contour[hull[i]]);
	}

	Mat hullContourMat(hullContour);

	return (contourArea(contourMat) / contourArea(hullContourMat));
}							

int Tracker::initTracker()
{
	const float minHandExtension = 0.2f; // in meters
	const double grabConvexity = 0.8;

	XnUserID userIds[maxUsers] = {0};
	XnUInt16 nUsers = maxUsers;
	char key = 0;

	// init context and generators
	initXtion(true, true, true);

	// register some handy callbacks
	SkeletonCapability skelCap = userGen.GetSkeletonCap();

	XnStatus nRetVal = XN_STATUS_OK;

	XnCallbackHandle hUserCallbacks, hCalibrationStart, hCalibrationComplete, hPoseDetected, hCalibrationInProgress, hPoseInProgress;
	if (!userGen.IsCapabilitySupported(XN_CAPABILITY_SKELETON))
	{
		printf("Supplied user generator doesn't support skeleton\n");
		return 1;
	}
	nRetVal = userGen.RegisterUserCallbacks(onNewUser, User_LostUser, this, hUserCallbacks);
	CHECK_RC(nRetVal, "Register to user callbacks");
	nRetVal = userGen.GetSkeletonCap().RegisterToCalibrationStart(UserCalibration_CalibrationStart, this, hCalibrationStart);
	CHECK_RC(nRetVal, "Register to calibration start");
	nRetVal = userGen.GetSkeletonCap().RegisterToCalibrationComplete(UserCalibration_CalibrationComplete, this, hCalibrationComplete);
	CHECK_RC(nRetVal, "Register to calibration complete");

	if (userGen.GetSkeletonCap().NeedPoseForCalibration())
	{
		g_bNeedPose = TRUE;
		if (!userGen.IsCapabilitySupported(XN_CAPABILITY_POSE_DETECTION))
		{
			printf("Pose required, but not supported\n");
			return 1;
		}
		nRetVal = userGen.GetPoseDetectionCap().RegisterToPoseDetected(UserPose_PoseDetected, this, hPoseDetected);
		CHECK_RC(nRetVal, "Register to Pose Detected");
		userGen.GetSkeletonCap().GetCalibrationPose(g_strPose);
	}

	nRetVal = userGen.GetSkeletonCap().RegisterToCalibrationInProgress(MyCalibrationInProgress, this, hCalibrationInProgress);
	CHECK_RC(nRetVal, "Register to calibration in progress");

	nRetVal = userGen.GetPoseDetectionCap().RegisterToPoseInProgress(MyPoseInProgress, this, hPoseInProgress);
	CHECK_RC(nRetVal, "Register to pose in progress");

	Mat mask(frameSize, CV_8UC1);

	while ( (key = (char) waitKey(1)) != 27 ) {
		context.WaitAndUpdateAll();

		// acquire bgr image
		{
			Mat mat(frameSize, CV_8UC3, (unsigned char*) imageGen.GetImageMap());
			cvtColor(mat, bgrMat, CV_RGB2BGR);
		}

		// acquire depth image
		{
			Mat mat(frameSize, CV_16UC1, (unsigned char*) depthGen.GetDepthMap());
			mat.copyTo(depthMat);
			depthMat.convertTo(depthMat8, CV_8UC1, 255.0f / 3000.0f);
			cvtColor(depthMat8, DepthMatbgr, CV_GRAY2BGR);
		}

		// iterate over all users
		nUsers = userGen.GetNumberOfUsers();
		userGen.GetUsers(userIds, nUsers);
		float rh[3]; // right hand coordinates (x[px], y[px], z[meters])
		float t[3]; // torso coordinates
		vector<Point> fingerTips; // fingertip coordinates

		for (int i=0; i<nUsers; i++) {
			int id = userIds[i];

			// torso
			if ( getJointImgCoordinates(skelCap, id, XN_SKEL_TORSO, t) == 1 ) {
				unsigned char shade = 255 - (unsigned char)(t[2] *  128.0f);
				circle(bgrMat, Point(t[0], t[1]), 10, Scalar(shade, 0, 0), -1);

				// right hand
				if ((getJointImgCoordinates(skelCap, id, XN_SKEL_RIGHT_HAND, rh) == 1) /* confident detection */ && 
					(rh[2] < t[2] - minHandExtension) /* user extends hand towards screen */  &&
					(rh[1] < t[1]) /* user raises his hand */) 
				{
					unsigned char shade = 255 - (unsigned char)(rh[2] *  128.0f);
					Scalar color(0, 0, shade);

					vector<Point> handContour;
					getHandContour(depthMat, rh, handContour);
					bool grasp = convexity(handContour) > grabConvexity;
					int thickness = grasp ? CV_FILLED : 3;
					circle(bgrMat, Point(rh[0], rh[1]), 10, color, thickness);

					cc.MoveMouse(rh[0], rh[1]);
					
					detectFingerTips(handContour, fingerTips, &bgrMat);
				}
			}
		}

		createViewer();
	}

	return 0;
}

void Tracker::createViewer()
{
	namedWindow("Tracker Viewer", CV_WINDOW_NORMAL);
	Size smaller(270,206);
	Mat newbgrMat(smaller, CV_8UC3);
	resize(bgrMat, newbgrMat, newbgrMat.size());
	imshow("Tracker Viewer", newbgrMat);
	//moveWindow("Tracker Viewer", GetSystemMetrics(SM_CXSCREEN)/1.25, GetSystemMetrics(SM_CYSCREEN)/1.55);
	HWND viewer = (HWND)cvGetWindowHandle("Tracker Viewer");
	SetWindowPos(viewer,HWND_TOPMOST,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
}

XnStatus Tracker::run()
{	
	Tracker track;
	int retVal = track.initTracker();
	if(retVal == 0)
	{
		return XN_STATUS_OK;
	}
}

Tracker::Tracker()
{
}

Tracker::~Tracker()
{
}